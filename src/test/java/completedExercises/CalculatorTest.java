package completedExercises;

import framework.AppiumTest;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CalculatorTest extends AppiumTest {

    @Test
    void should_multiply_two_and_two() {
        calculatorScreen.clickNumber(2);
        calculatorScreen.clickMultiply();
        calculatorScreen.clickNumber(2);
        calculatorScreen.clickEquals();

        assertThat(calculatorScreen.getResult())
                .as("Result of two times two should be four.")
                .isEqualTo(4);
    }

    @Test
    void should_divide_eight_by_four() {

        calculatorScreen.clickNumber(8);
        calculatorScreen.clickDivide();
        calculatorScreen.clickNumber(4);
        calculatorScreen.clickEquals();

        assertThat(calculatorScreen.getResult())
                .as("Result of eight divided by four should be two.")
                .isEqualTo(2);
    }

    @Test
    void should_subtract_six_from_ten_plus_seven() {
        calculatorScreen.clickNumber(1);
        calculatorScreen.clickNumber(0);
        calculatorScreen.clickPlus();
        calculatorScreen.clickNumber(7);
        calculatorScreen.clickMinus();
        calculatorScreen.clickNumber(6);
        calculatorScreen.clickEquals();

        assertThat(calculatorScreen.getResult())
                .as("Result of ten plus seven minus six should be eleven.")
                .isEqualTo(11);
    }

}
