package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

import static io.appium.java_client.MobileBy.AccessibilityId;
import static org.openqa.selenium.By.id;
import static org.openqa.selenium.By.xpath;

public class CalculatorScreen {

    private AppiumDriver<MobileElement> driver;
    private String digitButton = "digit_";
    private By resultBox = xpath("//android.widget.TextView[contains(@resource-id,'result')]");
    private By multiplyButton = xpath("//android.widget.Button[@text='×']");
    private By divideButton = AccessibilityId("divide");
    private By equalsButton = AccessibilityId("equals");
    private By plusButton = id("op_add");
    private By minusButton = id("op_sub");

    public CalculatorScreen(AppiumDriver<MobileElement> driver) {
        this.driver = driver;
    }

    public void clickNumber(int number) {
        driver.findElement(By.id(digitButton + number)).click();
    }

    public void clickEquals() {
        driver.findElement(equalsButton).click();
    }

    public void clickDivide() {
        driver.findElement(divideButton).click();
    }

    public void clickMultiply() {
        driver.findElement(multiplyButton).click();
    }

    public int getResult() {
        return Integer.parseInt(driver.findElement(resultBox).getText());
    }

    public void clickPlus() {
        driver.findElement(plusButton).click();
    }

    public void clickMinus() {
        driver.findElement(minusButton).click();
    }
}
