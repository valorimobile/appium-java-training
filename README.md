## appium-java-training

This is a project for the course 'Mobile App automation using Appium in Java'.
It tests the Android calculator app found in apps/ by performing a simple calculation.

## First use:
0. Import the project in IntelliJ IDEA or a different IDE if necessary.
0. Open a terminal (inside the IDE or outside it) and navigate to the project directory (if not already there).
1. Install the app on the preferred device with
`adb install apps/com.google.android.calculator.apk`.
2. Get the device name from the device with `adb devices` and fill it in in the application.properties at `DEVICE_NAME=`.
3. Start the Appium server.
4. Start the test by clicking the play-button next to `should_multiply_two_by_two` in `CalculatorTest`.
Alternatively, you can use the `mvn test` command in the terminal.
5. The test should pass.

## Creating a zipfile for students:
To avoid problems, make sure no uncommitted changes are outstanding. Run `./create_student_package.sh` in a terminal. The zipfile is created one directory higher.