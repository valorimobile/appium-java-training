git checkout student-framework
cd ..
zip -r appium-java-training-student.zip appium-java-training -x "*.DS_Store" -x "*.git*" -x "*.idea*" -x "*.iml*" -x "*target*" -x "*create_student_package.sh"
cd appium-java-training
git checkout master
